<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Auth::user()->akses == 1) {
                return abort(401);
            }

            return $next($request);
        });
    }

    public function index()
    {
        $product = Product::get();

        return view('cms.product.index')->with(['no' => 1, 'product' => $product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cms.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_produk' => 'required|unique:tbl_product',
            'img' => 'mimes:jpeg,png,jpg|max:5000',
        ]);

        $img = $request->file('img');
        $destinationPath = public_path('storage/img/product');
        if(!is_dir($destinationPath)){
            mkdir($destinationPath, 0755, true);
        }
        $imgName = time().'.'.$img->getClientOriginalExtension();
        $img->move($destinationPath, $imgName); 

        Product::create([ 
            'img' => $imgName,
            'nama_produk' => $request->nama_produk,
            'harga' => str_replace(".", "", $request->harga),
            'stock' => $request->stock,
        ]);

        return redirect()->route('product.index')->with('alert-success', "Data saved successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        return view('cms.product.show')->with(['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('cms.product.edit')->with(['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_produk' => 'required|unique:tbl_product,nama_produk,'.$id,
            'img' => 'mimes:jpeg,png,jpg|max:5000',
        ]);

        $form = [ 
            'nama_produk' => $request->nama_produk,
            'harga' => str_replace(".", "", $request->harga),
            'stock' => $request->stock,
        ];

        if ($request->img) {
            $img = $request->file('img');
            $destinationPath = public_path('storage/img/product');
            if(!is_dir($destinationPath)){
                mkdir($destinationPath, 0755, true);
            }
            $imgName = time().'.'.$img->getClientOriginalExtension();
            $img->move($destinationPath, $imgName); 
            $form['img'] = $imgName;
        }

        Product::where('id', $id)->update($form);

        return redirect()->route('product.index')->with('alert-success', "Data updated successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return redirect()->route('product.index')->with('alert-success', "Data deleted successfully!");
    }
}
