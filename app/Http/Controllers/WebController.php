<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use App\Models\Keranjang;
use Hash;
use Auth;
use DB;

class WebController extends Controller
{
    public function userRegister(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'nohp' => $request->nohp,
            'akses' => 1,
            'password' => Hash::make($request->password),
            'alamat' => $request->alamat,
        ]);

        $this->guard()->login($user);

        return redirect('/');
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function index()
    {
        if (Auth::user()) {
            if (Auth::user()->akses == 0) {
                return abort(401);
            }
        }

        $product = Product::get();

        return view('web.index')->with(['product' => $product]);
    }

    public function addToCart(Request $request)
    {
        Keranjang::create([
            'id_user' => Auth::user()->id,
            'id_produk' => $request->pid,
            'qty' => $request->qty,
            'status_checkout' => 'tidak',
        ]);

        return redirect('/')->with('alert-success', "Berhasil memasukkan produk ke dalam keranjang");
    }

    public function cart()
    {
        if (Auth::user()) {
            if (Auth::user()->akses == 0) {
                return abort(401);
            }
        }
        
        $total = 0;
        $data = DB::table('tbl_keranjang as a')->leftJoin('tbl_product as b', 'b.id', '=', 'a.id_produk')
                    ->where('a.status_checkout', "tidak")
                    ->select('a.qty', 'a.status_checkout', 'a.id as kid', 'b.*')
                    ->get();
        $dataCount = count($data);

        if ($dataCount == 1) {
            $total = $data[0]->harga*$data[0]->qty;
        } else {
            for ($i=0; $i < $dataCount; $i++) { 
                $total = $data[0]->harga*$data[0]->qty;

                $total = $total+$data[$i]->harga*$data[$i]->qty;
            }
        }

        return view('web.cart')->with(['data' => $data, 'total' => $total]);
    }

    public function checkout(Request $request)
    {
        for ($i=0; $i < count($request->kid); $i++) { 
            Keranjang::where('id', $request->kid[$i])->update(['status_checkout' => "ya"]);
            Product::where('id', $request->pid[$i])->update(['stock' => $request->stock[$i]-$request->qty[$i]]);
        }

        return back()->with('alert-success', "Produk berhasil di checkout!");
    }
}
