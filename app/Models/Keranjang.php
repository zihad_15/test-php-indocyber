<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    use HasFactory;

    protected $table = 'tbl_keranjang';

    protected $fillable = [
        'id_user',
        'id_produk',
        'qty',
        'status_checkout',
    ];

    public static function count($uid)
    {
        $keranjangCount = Keranjang::where('id_user', $uid)->where('status_checkout', 'tidak')->count();

        return $keranjangCount;
    }
}
