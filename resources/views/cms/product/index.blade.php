@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Product List
                    <a href="{{ route('product.create') }}" class="btn btn-sm btn-success float-right">+ add product</a>
                </div>

                <div class="card-body">
                    @if(session()->has('alert-success'))
                        <div class="alert alert-success">
                            {{ session()->get('alert-success') }}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered" style="width:100%" id="product">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Product Name</th>
                                <th>Stock</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($product as $v)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>
                                    <a href="{{ url('/') }}/storage/img/product/{{ $v->img }}" target="_blank"><img src="{{ url('/') }}/storage/img/product/{{ $v->img }}" width="80" height="80"></a>
                                </td>
                                <td>{{ $v->nama_produk }}</td>
                                <td>{{ $v->stock }}</td>
                                <td>Rp{{ number_format($v->harga, 0, ".", ".") }}</td>
                                <td>
                                    <form action="{{ route('product.destroy',$v->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{ route('product.show',$v->id) }}" class="btn btn-sm btn-success">View</a>
                                        <a href="{{ route('product.edit',$v->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus produk tersebut!')">Delete</button>
                                    </form>
                                </td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#product').DataTable();
        });
    </script>
@endsection
