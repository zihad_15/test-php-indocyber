@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Product Detail
                </div>

                <div class="card-body">
                    <form>
                        <img src="{{ url('/') }}/storage/img/product/{{ $product->img }}" alt="" style="width: 100px;height: 100px;margin-bottom: 10px;display:">
                      <div class="form-group">
                        <label for="#">Product Name</label>
                        <input type='text' name="nama_produk" class="form-control" value="{{ $product->nama_produk }}" disabled />
                      </div>
                      <div class="form-group">
                        <label for="#">Stock</label>
                        <input type='number' name="stock" class="form-control" value="{{ $product->stock }}" disabled />
                      </div>
                      <div class="form-group">
                        <label for="#">Price</label>
                        <input type='text' name="harga" class="form-control" value="Rp{{ number_format($product->harga, 0, ".", ".") }}" disabled />
                      </div>
                      <hr>
                      <div class="float-right">
                        <a href="{{ route('product.index') }}" class="btn btn-sm btn-success">Back</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(input).parent().children("img").show()
                        .attr('src', e.target.result)
                        .width(100)
                        .height(100);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#harga').mask("#.##0", {reverse: true});
    </script>
@endsection