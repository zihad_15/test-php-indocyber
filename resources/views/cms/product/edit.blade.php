@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Product Edit
                </div>

                <div class="card-body">
                    @if($errors->any())
                        {!! implode('', $errors->all('<div class="alert alert-danger" role="alert">:message</div>')) !!}
                    @endif
                    <form action="{{ route('product.update',$product->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                      <div class="form-group">
                        <label for="#">Image</label><br>
                        <img src="{{ url('/') }}/storage/img/product/{{ $product->img }}" alt="" style="width: 100px;height: 100px;margin-bottom: 10px;display:">
                        <input type='file' name="img" id="img" onchange="readURL(this);" class="form-control" accept="image/*"/>
                      </div>
                      <div class="form-group">
                        <label for="#">Product Name</label>
                        <input type='text' name="nama_produk" class="form-control" value="{{ $product->nama_produk }}" required />
                      </div>
                      <div class="form-group">
                        <label for="#">Stock</label>
                        <input type='number' name="stock" class="form-control" value="{{ $product->stock }}" required />
                      </div>
                      <div class="form-group">
                        <label for="#">Price</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                              <div class="input-group-text">Rp</div>
                            </div>
                            <input type='text' name="harga" id="harga" class="form-control" value="{{ $product->harga }}" required />
                        </div>
                      </div>
                      <hr>
                      <div class="float-right">
                        <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        <a href="{{ route('product.index') }}" class="btn btn-sm btn-danger">Cancel</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(input).parent().children("img").show()
                        .attr('src', e.target.result)
                        .width(100)
                        .height(100);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#harga').mask("#.##0", {reverse: true});
    </script>
@endsection