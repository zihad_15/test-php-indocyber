<!-- ***** Header Start ***** -->
<header id="header" class="odd">
    <!-- Navbar -->
    <nav data-aos="zoom-out" data-aos-delay="800" class="navbar navbar-expand">
        <div class="container header">
            <!-- Navbar Brand-->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="navbar-brand-sticky" src="{{ url('/') }}/storage/assets/img/logo/logo-white.png" alt="sticky brand-logo">
            </a>
            <div class="ml-auto"></div>
            <!-- Navbar -->
            <ul class="navbar-nav items mx-auto">
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link" href="index.html">Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#">Explore <i class="fas fa-angle-down ml-1"></i></a>
                    <ul class="dropdown-menu">
                        <li class="nav-item"><a href="explore-1.html" class="nav-link">Explore Style 1</a></li>
                        <li class="nav-item"><a href="explore-2.html" class="nav-link">Explore Style 2</a></li>
                        <li class="nav-item"><a href="explore-3.html" class="nav-link">Explore Style 3</a></li>
                        <li class="nav-item"><a href="explore-4.html" class="nav-link">Explore Style 4</a></li>
                        <li class="nav-item"><a href="auctions.html" class="nav-link">Live Auctions</a></li>
                        <li class="nav-item"><a href="item-details.html" class="nav-link">Item Details</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="activity.html" class="nav-link">Activity</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#">Community <i class="fas fa-angle-down ml-1"></i></a>
                    <ul class="dropdown-menu">
                        <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li>
                        <li class="nav-item"><a href="blog-single.html" class="nav-link">Blog Single</a></li>
                        <li class="nav-item"><a href="help-center.html" class="nav-link">Help Center</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#">Pages <i class="fas fa-angle-down ml-1"></i></a>
                    <ul class="dropdown-menu">
                        <li class="nav-item"><a href="authors.html" class="nav-link">Authors</a></li>
                        <li class="nav-item"><a href="author.html" class="nav-link">Author</a></li>
                        <li class="nav-item"><a href="wallet-connect.html" class="nav-link">Wallet Connect</a></li>
                        <li class="nav-item"><a href="create.html" class="nav-link">Create</a></li>
                        <li class="nav-item"><a href="login.html" class="nav-link">Login</a></li>
                        <li class="nav-item"><a href="signup.html" class="nav-link">Signup</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="contact.html" class="nav-link">Contact</a>
                </li> -->
            </ul>
            <!-- Navbar Icons -->
            <!-- <ul class="navbar-nav icons">
                <li class="nav-item">
                    <a href="#" class="nav-link" data-toggle="modal" data-target="#search">
                        <i class="fas fa-search"></i>
                    </a>
                </li>
            </ul> -->

            <!-- Navbar Toggler -->
            <ul class="navbar-nav toggle">
                <li class="nav-item">
                    <a href="#" class="nav-link" data-toggle="modal" data-target="#menu">
                        <i class="fas fa-bars toggle-icon m-0"></i>
                    </a>
                </li>
            </ul>

            @guest
                <!-- Navbar Action Button -->
                <ul class="navbar-nav action">
                    <li class="nav-item ml-3">
                        <a href="javascript:void(0)" class="btn ml-lg-auto btn-bordered-white" data-toggle="modal" data-target="#modalLogin">Login</a>
                        <!-- <a href="wallet-connect.html" class="btn ml-lg-auto btn-bordered-white">
                            <i class="icon-wallet mr-md-2"></i>
                            Wallet Connect
                        </a> -->
                    </li>
                    <li class="nav-item ml-3">
                        <a href="javascript:void(0)" class="btn ml-lg-auto btn-bordered-white" data-toggle="modal" data-target="#modalRegister">Register</a>
                        <!-- <a href="wallet-connect.html" class="btn ml-lg-auto btn-bordered-white">
                            <i class="icon-wallet mr-md-2"></i>
                            Wallet Connect
                        </a> -->
                    </li>
                </ul>
            @else
                <ul class="navbar-nav action">
                    <li class="nav-item ml-3">
                        <a href="{{ url('/') }}" style="color: white;">Browse</a>
                    </li>
                    <li class="navbar-nav dropdown">
                        <a class="nav-link" href="#">{{ Auth::user()->name }} <i class="fas fa-angle-down ml-1"></i></a>
                        <ul class="dropdown-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" style="color: red;">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item ml-3">
                        <a href="{{ url('cart') }}" style="color: white;"><i class="icon-handbag mr-2"></i> {{ App\Models\Keranjang::count(Auth::user()->id) }}</a>
                    </li>
                </ul>
            @endguest
        </div>
    </nav>
</header>
<!-- ***** Header End ***** -->

<!-- Modal Login -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="padding: 1rem; border-bottom: 1px solid #dee2e6;">
        <h5 class="modal-title" id="exampleModalLabel">Login</h5>
      </div>
      <div class="modal-body">
        <form action="{{ route('login') }}" method="POST">
            @csrf
          <div class="form-group">
            <label for="">Email</label>
            <input type="email" name="email" class="form-control" maxlength="50" placeholder="-" required>
          </div>
          <div class="form-group">
            <label for="">Password</label>
            <input type="password" name="password" id="password" class="form-control" placeholder="-" required>
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Login</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Register -->
<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="padding: 1rem; border-bottom: 1px solid #dee2e6;">
        <h5 class="modal-title" id="exampleModalLabel">Register</h5>
      </div>
      <div class="modal-body">
        <form action="{{ url('userRegister') }}" method="POST" id="register">
            @csrf
          <div class="form-group">
            <label for="">Email</label>
            <input type="email" name="email" class="form-control" maxlength="50" placeholder="-" required>
          </div>
          <div class="form-group">
            <label for="">Name</label>
            <input type="text" name="name" class="form-control" placeholder="-" required>
          </div>
          <div class="form-group">
            <label for="">Password</label>
            <input type="password" name="password" id="password" class="form-control" pattern="(?=.*\d)(?=.*[!$#%.])(?=.*[a-z])(?=.*[A-Z]).{6,}" title="Must contain at least one number and one uppercase and one lowercase letter and non-alphanumeric (Example: !, $, #, or %, and at least 6 or more characters" placeholder="-" required>
          </div>
          <div class="form-group">
            <label for="">Confirm Password</label>
            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" required>
          </div>
          <div class="form-group">
            <label for="">No Hp</label>
            <input type="number" name="nohp" class="form-control" placeholder="-" required>
          </div>
          <div class="form-group">
            <label for="">Alamat</label>
            <input type="text" name="alamat" class="form-control" placeholder="-" required>
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" onclick="registerValidationCheck()">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>
@section('script')
    <script type="text/javascript">
        function registerValidationCheck() {
            if ($('#password').val() != $('#password_confirmation').val()) {
                event.preventDefault();
                alert('Password dan confirmation tidak cocok!');
                return false;
            }
        }
    </script>
@endsection