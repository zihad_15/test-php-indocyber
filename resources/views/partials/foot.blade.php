<!-- ***** All jQuery Plugins ***** -->

<!-- jQuery(necessary for all JavaScript plugins) -->
<script src="{{ url('/') }}/storage/assets/js/vendor/jquery.min.js"></script>

<!-- Bootstrap js -->
<script src="{{ url('/') }}/storage/assets/js/vendor/popper.min.js"></script>
<script src="{{ url('/') }}/storage/assets/js/vendor/bootstrap.min.js"></script>

<!-- Plugins js -->
<script src="{{ url('/') }}/storage/assets/js/vendor/all.min.js"></script>
<script src="{{ url('/') }}/storage/assets/js/vendor/slider.min.js"></script>
<script src="{{ url('/') }}/storage/assets/js/vendor/countdown.min.js"></script>
<script src="{{ url('/') }}/storage/assets/js/vendor/shuffle.min.js"></script>

<!-- Active js -->
<script src="{{ url('/') }}/storage/assets/js/main.js"></script>
@yield('script')