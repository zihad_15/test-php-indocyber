@extends('layouts.app_web')
@section('content')
    <!-- ***** Breadcrumb Area Start ***** -->
    <section class="breadcrumb-area overlay-dark d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Breamcrumb Content -->
                    <div class="breadcrumb-content text-center">
                        <h2 class="m-0">Explore Our Marketplace</h2>
                        <!-- <ol class="breadcrumb d-flex justify-content-center">
                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Explore</a></li>
                            <li class="breadcrumb-item active">Explore Style 1</li>
                        </ol> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Breadcrumb Area End ***** -->

    <!-- ***** Explore Area Start ***** -->
    <section class="explore-area">
        <div class="container">
            @if(session()->has('alert-success'))
                <div class="alert alert-success">
                    {{ session()->get('alert-success') }}
                </div>
            @endif
            <div class="row">
                <div class="col-12">
                    <!-- Intro -->
                    <div class="intro d-flex justify-content-between align-items-end m-0">
                        <div class="intro-content">
                            <span>Exclusive Product</span>
                            <h3 class="mt-3 mb-0">@if(count($product) < 1) No products listed yet @else Explore @endif</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row items">
                @foreach($product as $v)
                    <div class="col-12 col-sm-6 col-lg-3 item">
                        <div class="card">
                            <div class="image-over">
                                <a href="javascript:void(0)">
                                    <img class="card-img-top" src="{{ url('/') }}/storage/img/product/{{ $v->img }}" alt="">
                                </a>
                            </div>
                            <!-- Card Caption -->
                            <div class="card-caption col-12 p-0">
                                <!-- Card Body -->
                                <div class="card-body">
                                    <a href="javascript:void(0)">
                                        <h5 class="mb-0">{{ $v->nama_produk }}</h5>
                                    </a>
                                    <br>
                                    <div class="card-bottom d-flex justify-content-between">
                                        <span>Rp{{ number_format($v->harga, 0, ".", ".") }}</span>
                                        <span>1 of {{ $v->stock }}</span>
                                    </div>
                                    @guest
                                        <a class="btn btn-bordered btn-smaller mt-3" href="javascript:void(0)" data-toggle="modal" data-target="#modalLogin"><i class="icon-handbag mr-2"></i>login to add cart</a>
                                    @else
                                        <a class="btn btn-bordered btn-smaller mt-3" href="javascript:void(0)" data-toggle="modal" data-target="#modalAddCart-{{ $v->id }}"><i class="icon-handbag mr-2"></i>add to cart</a>
                                    @endguest
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Add Cart -->
                    <div class="modal fade" id="modalAddCart-{{ $v->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header" style="padding: 1rem; border-bottom: 1px solid #dee2e6;">
                            <h5 class="modal-title" id="exampleModalLabel">Add to cart</h5>
                          </div>
                          <div class="modal-body">
                            <form action="{{ url('addToCart') }}" method="POST" id="formAddToChart-{{ $v->id }}">
                                @csrf
                                <img src="{{ url('/') }}/storage/img/product/{{ $v->img }}" alt="" style="width: 100px;height: 100px;margin-bottom: 10px;display:">
                              <div class="form-group">
                                <label for="#">Product Name</label>
                                <input type='text' name="nama_produk" class="form-control" value="{{ $v->nama_produk }}" disabled />
                              </div>
                              <div class="form-group">
                                <label for="#">Stock</label>
                                <input type='number' name="stock" id="stock" class="form-control" value="{{ $v->stock }}" disabled />
                              </div>
                              <div class="form-group">
                                <label for="#">Price</label>
                                <input type='text' name="harga" class="form-control" value="Rp{{ number_format($v->harga, 0, ".", ".") }}" disabled />
                              </div>
                              <div class="form-group">
                                <label for="#">Qty</label>
                                <input type='number' name="qty" id="qty-{{ $v->id }}" class="form-control" placeholder="-" />
                              </div>
                              <input type='hidden' name="pid" value="{{ $v->id }}" />
                          </div>
                          <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <a class="btn btn-primary" href="javascript:void(0)"  onclick="qtyValidationCheck({{ $v->id }}, {{ $v->stock }})"><i class="icon-handbag mr-2"></i>add to cart</a>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- ***** Explore Area End ***** -->
@endsection
@section('script')
    <script type="text/javascript">
        function qtyValidationCheck(id, stock) {

            if ($('#qty-'+id).val() > stock) {
                alert('Qty melebihi stock yang tersedia!');
                return false;
            }

            $('#formAddToChart-'+id).submit();
        }
    </script>
@endsection