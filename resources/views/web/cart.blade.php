@extends('layouts.app_web')
@section('content')
    <!-- ***** Breadcrumb Area Start ***** -->
    <section class="breadcrumb-area overlay-dark d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Breamcrumb Content -->
                    <div class="breadcrumb-content text-center">
                        <h2 class="m-0">Your Cart</h2>
                        <!-- <ol class="breadcrumb d-flex justify-content-center">
                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Explore</a></li>
                            <li class="breadcrumb-item active">Explore Style 1</li>
                        </ol> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Breadcrumb Area End ***** -->

    <!-- ***** Explore Area Start ***** -->
    <section class="explore-area">
        <div class="container">
            @if(session()->has('alert-success'))
                <div class="alert alert-success">
                    {{ session()->get('alert-success') }}
                </div>
            @endif
            <div class="row">
                <div class="col-12">
                    <!-- Intro -->
                    <div class="intro d-flex justify-content-between align-items-end m-0">
                        <div class="intro-content">
                            <h3 class="mt-3 mb-0">Cart</h3>
                        </div>
                    </div>
                </div>
            </div>
            <form action="{{ url('checkout') }}" method="POST" id="formCheckout">
                <div class="row items">
                    @csrf
                    @foreach($data as $v)
                        <input type="hidden" name="kid[]" value="{{ $v->kid }}">
                        <input type="hidden" name="pid[]" value="{{ $v->id }}">
                        <input type="hidden" name="stock[]" value="{{ $v->stock }}">
                        <input type="hidden" name="qty[]" value="{{ $v->qty }}">
                        <div class="col-12 col-sm-6 col-lg-3 item">
                            <div class="card">
                                <div class="image-over">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top" src="{{ url('/') }}/storage/img/product/{{ $v->img }}" alt="">
                                    </a>
                                </div>
                                <!-- Card Caption -->
                                <div class="card-caption col-12 p-0">
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <a href="javascript:void(0)">
                                            <h5 class="mb-0">{{ $v->nama_produk }}</h5>
                                        </a>
                                        <br>
                                        <div class="card-bottom d-flex justify-content-between">
                                            <span>Rp{{ number_format($v->harga, 0, ".", ".") }}</span>
                                            <span>Qty: {{ $v->qty }}</span>
                                        </div>
                                        <br>
                                        <div class="card-bottom d-flex justify-content-between">
                                            <span>Subtotal: Rp{{ number_format($v->harga*$v->qty, 0, ".", ".") }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-12 col-sm-6 col-lg-3 item">
                        <div class="card">
                            <!-- Card Caption -->
                            <div class="card-caption col-12 p-0">
                                <!-- Card Body -->
                                <div class="card-body">
                                    <a href="javascript:void(0)">
                                        <h5 class="mb-0">Total: Rp{{ number_format($total, 0, ".", ".") }}</h5>
                                    </a>
                                </div>
                                <div class="modal-footer">
                                    @if($total > 0)
                                        <a class="btn btn-primary" href="javascript:void(0)" onclick="checkout()">Checkout</a>
                                    @else
                                        <h6>Anda belum memasukkan apapun kedalam keranjang</h6>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!-- ***** Explore Area End ***** -->
@endsection
@section('script')
    <script type="text/javascript">
        function checkout() {
            var r = confirm("Apakah produk yang ingin dibeli sudah benar?");
            if (r) {
                $('#formCheckout').submit();
            }            
        }
    </script>
@endsection