<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\WebController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('product', ProductController::class);

Route::get('/', [WebController::class, 'index']);
Route::post('/userRegister', [WebController::class, 'userRegister']);
Route::post('/addToCart', [WebController::class, 'addToCart']);
Route::get('/cart', [WebController::class, 'cart'])->middleware('auth');
Route::post('/checkout', [WebController::class, 'checkout'])->middleware('auth');